import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

const big = const TextStyle(fontSize: 30);
const body = const TextStyle(fontSize: 20);

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: MyHomePage(title: 'How Many Bags?'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
      ),
      endDrawer: Drawer(
        child: ListView(
          padding: const EdgeInsets.all(8),
          children: <Widget>[
            Container(
              height: 100,
              color: Colors.blue[600],
              child: const Center(child: Text('Profile', style: big)),
            ),
            Container(
              height: 100,
              color: Colors.red[600],
              child: const Center(child: Text('Settings', style: big)),
            ),
            Container(
              height: 100,
              color: Colors.yellow[600],
              child: const Center(child: Text('Payment Info', style: big)),
            ),
            Container(
              height: 100,
              color: Colors.green[600],
              child: const Center(child: Text('Support', style: big)),
            ),
          ],
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
                'The driver will collect this many bags:', style: body
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}